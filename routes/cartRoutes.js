const express = require("express");
const {
  addToCart,
  removeFromCart,
  emptyCart,
  addQuantity,
  subtractQuantity,
} = require("../controllers/cartController");
const cartRouter = express();

cartRouter.post("/add_to_cart", (req, res) => {
  addToCart(req, res);
});

cartRouter.delete("/remove_from_cart", (req, res) => {
  removeFromCart(req, res);
});

cartRouter.delete("/empty_cart", (req, res) => {
  emptyCart(req, res);
});

cartRouter.post("/add_quantity", (req, res) => {
  addQuantity(req, res);
});

cartRouter.post("/subtract_quantity", (req, res) => {
  subtractQuantity(req, res);
});

module.exports = cartRouter;
