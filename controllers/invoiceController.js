// index.js
const { createInvoice } = require("../pdf generator/createInvoice.js");
const invoiceModel = require("../models/invoiceModel");

module.exports.addInvoice = async (request, response) => {
  const invoice = new invoiceModel(request.body);
  try {
    await invoice.save();
    createInvoice(invoice, "../react_app/src/invoice.pdf");
    response.status(200).send(invoice);
  } catch (error) {
    response.status(500).send(error);
  }
};
