const express = require("express");
const { validateToken } = require("../auth/validateToken");
const {
  addProduct,
  getAllProducts,
  getAProduct,
} = require("../controllers/productController");

const productRouter = express();

productRouter.post("/add_product", (req, res) => {
  addProduct(req, res);
});

productRouter.get("/products", validateToken,(req, res) => {
  getAllProducts(req, res);
});

productRouter.get("/product/:product_id", (req, res) => {
  getAProduct(req, res);
});

module.exports = productRouter;
