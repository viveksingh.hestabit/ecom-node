const jwt = require("jsonwebtoken");

function validateToken(req, res, next) {
    const authHeader = req.headers["authorization"]
    if (authHeader == null)
        res.sendStatus(400).send("please login");
    const token = authHeader.split(" ")[0]
    if (token == null)
        res.sendStatus(400).send("Token not present")
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if (err) {
            res.status(403).send("Token invalid")
        } else {
            req.user = user
            next() //proceed to the next action in the calling function
        }
    }) 
    //end of jwt.verify()
} 
//end of function

module.exports = {validateToken};