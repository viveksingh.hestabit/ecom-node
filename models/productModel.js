const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
    maxLength: 200,
  },
  prod_img_url: {
    type: String,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
  stock: {
    type: Number,
    required: true,
  },
  colorOptions: [
    {
      url: {
        type: String,
      },
      color: {
        type: String,
      },
    },
  ],
});

const Product = mongoose.model("Product", ProductSchema);

module.exports = Product;
