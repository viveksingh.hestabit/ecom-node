const Carousel = require("../models/carouselModel");

module.exports.getAllCarouselImages = async (req, res) => {
  try {
    const images = await Carousel.find({});
    if (!images) {
      return res.json({ message: "can't find any carousel images" });
    } else {
      return res.status(200).send(images);
    }
  } catch (err) {
    res.send(err);
  }
};

module.exports.addCarouselImage = async (req, res) => {
  const { description, img, name } = req.body;
  try {
    const check = await Carousel.findOne({ name: name });
    if (check) {
      return res.json({ message: "image name already exists" });
    } else {
      const newImg = await Carousel.create({
        description: description,
        img: img,
        name: name,
      });
      await newImg.save();
      return res.json({ message: "image added successfully", imgData: newImg });
    }
  } catch (err) {
    res.send(err);
  }
};
