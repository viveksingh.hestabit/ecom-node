module.exports.generateArr = (req, res) => {
  const arr = req.body.prod;
  const itemArr = [];
  let totalPrice = 0;

  arr.map((item) => {
    itemArr.push({
      name: item.name,
      description: item.description,
      unit_amount: {
        currency_code: "USD",
        value: item.price,
      },
      quantity: item.quantity,
    });
  });
  arr.map((item) => {
    totalPrice += Number(item.price);
  });
  res.send({ itemArr, totalPrice });
};
