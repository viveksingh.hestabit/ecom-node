const express = require("express");
const {
  getAllCarouselImages,
  addCarouselImage,
} = require("../controllers/carouselController");

const carouselRouter = express();

carouselRouter.get("/carousel_images", (req, res) => {
  getAllCarouselImages(req, res);
});

carouselRouter.post("/add_carousel_image", (req, res) => {
  addCarouselImage(req, res);
});

module.exports = carouselRouter;
