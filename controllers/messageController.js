const User = require("../models/userModel");
const Message = require("../models/messageModel");

module.exports.addMessage = async (req, res) => {
  const { content, chatId, contentType } = req.body;
  if (!content || !chatId) {
    res.status(400).json({ message: "invalid data passed into body" });
  }
  let newMsg = {
    sender: req.body.sender_id,
    content: content,
    chat: chatId,
    contentType:contentType
  };
  try {
    let msg = await Message.create(newMsg);
    msg = await (await msg.populate("sender", "name email")).populate("chat");
    // msg = await msg.populate("chat");
    msg = await User.populate(msg, {
      path: "chat.users",
      select: "name",
    });
    res.status(200).send(msg);
  } catch (err) {
    res.status(400).send(err);
  }
};

module.exports.allMessagesOfAChat = async (req, res) => {
  try {
    const messages = await Message.find({ chat: req.params.chat_id })
      .populate("sender", "name email img_url")
      .populate("chat");
    res.send(messages);
  } catch (err) {
    res.send(err);
  }
};
