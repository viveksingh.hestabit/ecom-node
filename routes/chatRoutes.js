const express = require("express");
const { addChat  } = require("../controllers/chatController");

const chatRouter = express();

chatRouter.post("/add_new_chat", (req, res) => {
  addChat(req, res);
});

module.exports = chatRouter;
