const productModel = require("../models/productModel");

module.exports.addProduct = async (request, response) => {
  const product = new productModel(request.body);
  try {
    await product.save();
    response.send(product);
  } catch (error) {
    response.status(500).send(error);
  }
};

module.exports.getAllProducts = async (request, response) => {
  const products = await productModel.find({});
  try {
    response.send(products);
  } catch (error) {
    response.status(500).send(error);
  }
};

module.exports.getAProduct = async (request, response) => {
  const productId = request.params.product_id;
  try {
    const product = await productModel.findById(productId);
    response.json(product);
  } catch (error) {
    response.status(500).send(error);
  }
};
