const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  ID: {
    type: String,
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    validate: {
      validator: function (v) {
        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v);
      },
      message: "Please enter a valid email",
    },
    required: [true, "Email required"],
  },
  img_url: {
    type: String,
  },
  status: {
    type: Boolean,
    default: false,
  },
  SID: {
    type: String,
  },
  password: {
    type: String,
  },
  cart: [
    {
      product_id: {
        type: String,
      },
      price: {
        type: String,
      },
      name: {
        type: String,
      },
      description: {
        type: String,
      },
      img_url: {
        type: String,
      },
      quantity: {
        type: Number,
      },
    },
  ],
});

const User = mongoose.model("User", UserSchema);

module.exports = User;
