const mongoose = require("mongoose");

const InvoiceSchema = new mongoose.Schema({
  customerDetails: {
    name: {
      type: String,
      // required: true,
    },
    email: {
      type: String,
      trim: true,
      // lowercase: true,
      // unique: true,
      validate: {
        validator: function (v) {
          return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v);
        },
        message: "Please enter a valid email",
      },
      required: [true, "Email required"],
    },
    address: {
      line1: {
        type: String,
      },
      line2: {
        type: String,
      },
      city: {
        type: String,
      },
      state: {
        type: String,
      },
      country: {
        type: String,
      },
      postalCode: {
        type: String,
      },
    },
    phoneNo: {
      type: String,
    },
  },
  orderDetails: {
    invoiceId: {
      type: String,
      unique: true,
    },
    invoiceDate: {
      type: String,
    },
    transactionId: {
      type: String,
    },
    paidThrough: {
      type: String,
    },
    products: [
      {
        name: {
          type: String,
        },
        description: {
          type: String,
        },
        quantity: {
          type: Number,
        },
        price: {
          type: String,
        },
      },
    ],
    subTotal: {
      type: Number,
    },
    tax: {
      type: Number,
    },
    delivery: {
      type: Number,
    },
    totalAmount: {
      type: Number,
    },
  },
});

const Invoice = mongoose.model("Invoice", InvoiceSchema);

module.exports = Invoice;
