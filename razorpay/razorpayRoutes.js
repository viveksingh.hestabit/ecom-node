const express = require("express");
const {
  createOrderRazorpay,
  verifyPayment,
  generateInvoice,
} = require("./razorpayHandler");

const razorpayRouter = express();

razorpayRouter.post("/api/payment/orders", async (req, res) => {
  createOrderRazorpay(req, res);
});

razorpayRouter.post("/api/payment/verify", async (req, res) => {
  verifyPayment(req, res);
});

razorpayRouter.get("/api/payment/invoices", async (req, res) => {
  generateInvoice(req, res);
});

module.exports = razorpayRouter;
