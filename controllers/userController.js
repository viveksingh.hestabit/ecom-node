const userModel = require("../models/userModel");
const bcrypt = require("bcrypt");
const { generateAccessToken, generateRefreshToken } = require("../auth/createToken");
const refreshTokens = require("../auth/createToken");
const saltRounds = 10;

const emailValidator = (email) => {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
};

module.exports.addUser = async (req, res) => {
  const { password, confirmPassword, ID, email, name, img_url } = req.body;

  if (!emailValidator(email)) {
    return res.json({ message: "enter a valid email", success: false });
  }

  if (password !== confirmPassword) {
    res.json({ message: "passwords do not match", success: false });
  } else {
    try {
      const IdExists = await userModel.find({ ID: ID });
      if (IdExists.length > 0) {
        res.json({ message: "username already present", success: false });
      } else {
        try {
          bcrypt.hash(password, saltRounds, async (err, hash) => {
            if (err) {
              res.send(err);
            } else {
              const data = {
                name: name,
                email: email,
                password: hash,
                ID: ID,
                img_url: img_url,
              };
              const user = new userModel(data);
              await user.save();
              res.send({ user: user, success: true });
            }
          });
        } catch (err) {
          res.send(err);
        }
      }
    } catch (err) {
      res.send(err);
    }
  }
};

module.exports.getAllUsers = async (request, response) => {
  const users = await userModel.find({});
  try {
    response.send(users);
  } catch (error) {
    response.status(500).send(error);
  }
};

module.exports.getAUser = async (request, response) => {
  const userId = request.params.user_id;
  try {
    const user = await userModel.findOne({ ID: userId });
    response.send(user);
  } catch (error) {
    response.status(500).send(error);
  }
};

module.exports.loginChatUser = async (req, res) => {
  const { password, email } = req.body;
  try {
    const user = await userModel.findOne({ email: email });
    if (user) {
      try {
        bcrypt.compare(password, user.password, (err, result) => {
          if (err) {
            res.send(err);
          } else {
            if (!result) {
              res.json({ message: "wrong password", success: false });
            } else {
              const accessToken = generateAccessToken({user});
              const refreshToken = generateRefreshToken({user});
              res.status(200).send({ user: user,accessToken:accessToken,refreshToken:refreshToken, success: true });
            }
          }
        });
      } catch (err) {
        res.send(err);
      }
    } else {
      res.json({ message: "invalid username or password", success: false });
    }
  } catch (err) {
    res.send(err);
  }
};

module.exports.searchChatUser = async (req, res) => {
  const keyword = req.query.search
    ? {
        $or: [
          {
            name: { $regex: req.query.search, $options: "i" },
          },
          // { email: { $regex: req.query.search, $options: "i" } },
        ],
      }
    : {};

  const users = await userModel.find(keyword).find({
    ID: { $ne: req.params.id },
  });
  res.send(users);
};

module.exports.logoutChatUser = async (req, res) => {
  try {
    await userModel.findOneAndUpdate(
      { ID: req.body.id },
      { $set: { status: false, SID: "" } }
    );
    const user = await userModel.findOne({ ID: req.body.id });
    res.status(200).send(user);
  } catch (err) {
    res.send(err);
  }
};

module.exports.getCurrentChattingUser = async (req, res) => {
  const id = req.params.id;
  try {
    const user = await userModel.findById(id);
    res.status(200).send(user);
  } catch (err) {
    res.status(500).json({ message: "can't find user" });
  }
};

// module.exports.refreshToken = async (req, res) => {
//   // return refreshTokens;
//   console.log(refreshTokens)
//   // return res.send(refreshTokens);
//   if (!refreshTokens.includes(req.body.token))
//     res.status(400).send("Refresh Token Invalid")
//   refreshTokens = refreshTokens.filter((c) => c != req.body.token)
//   //remove the old refreshToken from the refreshTokens list
//   const accessToken = generateAccessToken({ user: req.body.name })
//   const refreshToken = generateRefreshToken({ user: req.body.name })
//   //generate new accessToken and refreshTokens
//   res.json({ accessToken: accessToken, refreshToken: refreshToken })
// };