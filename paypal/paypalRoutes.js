const express = require("express");
const { generateArr } = require("./paypalHandler");

const paypalRouter = express();

paypalRouter.post("/generate_paypal_checkout", (req, res) => {
  generateArr(req, res);
});

module.exports = paypalRouter;
