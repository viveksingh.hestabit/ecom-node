const jwt = require("jsonwebtoken");

generateAccessToken=(user)=> {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: "15m" })
}
// refreshTokens
const refreshTokens = []
generateRefreshToken=(user)=>{
    const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, { expiresIn: "20m" })
        refreshTokens.push(refreshToken)
    return refreshTokens
}
module.exports = {refreshTokens,generateRefreshToken,generateAccessToken};