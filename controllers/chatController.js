const Chat = require("../models/chatModel");
const User = require("../models/userModel");

module.exports.addChat = async (req, res) => {
  const { loggedInUserId, reciverUserId } = req.body;
  if (!loggedInUserId || !reciverUserId) {
    res.status(400).json({ message: "Body data is not valid" });
  }
  let isChat = await Chat.find({
    $and: [
      { users: { $elemMatch: { $eq: loggedInUserId } } },
      { users: { $elemMatch: { $eq: reciverUserId } } },
    ],
  }).populate("users");
  isChat = await User.populate(isChat, {
    path: "latestMessage.sender",
    select: "name, email, img_url",
  });
  if (isChat.length > 0) {
    res.send(isChat[0]);
  } else {
    const chatData = {
      chatName: "sender",
      users: [loggedInUserId, reciverUserId],
    };
    try {
      const createdChat = await Chat.create(chatData);
      const fullChat = await Chat.find({ _id: createdChat._id }).populate(
        "users"
      );
      res.status(200).send(fullChat);
    } catch (err) {
      res.status(500).send(err);
    }
  }
};

