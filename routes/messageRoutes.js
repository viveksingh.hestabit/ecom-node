const express = require("express");
const {
  addMessage,
  allMessagesOfAChat,
} = require("../controllers/messageController");

const msgRouter = express();

msgRouter.post("/add_new_msg", (req, res) => {
  addMessage(req, res);
});

msgRouter.get("/chat/:chat_id", (req, res) => {
  allMessagesOfAChat(req, res);
});

module.exports = msgRouter;
