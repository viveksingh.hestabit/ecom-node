const fs = require("fs");
const PDFDocument = require("pdfkit");

async function createInvoice(invoice, path) {
  let doc = new PDFDocument({ size: "A4", margin: 50 });

  generateHeader(doc);
  generateCustomerInformation(doc, invoice);
  generateInvoiceTable(doc, invoice);
  generateFooter(doc);

  doc.end();
  doc.pipe(fs.createWriteStream(path));
}

function generateHeader(doc) {
  doc
    .image("website_logo.png", 50, 45, { width: 50 })
    .fillColor("#00dcff")
    .fontSize(20)
    .text("Letz Decor", 110, 57)
    .fillColor("#444444")
    .fontSize(10)
    .text("Letz Decor pvt ltd.", 200, 50, { align: "right" })
    .text("123 Main Street", 200, 65, { align: "right" })
    .text("Noida, IN, 101025", 200, 80, { align: "right" })
    .moveDown();
}

function generateCustomerInformation(doc, invoice) {
  doc.fillColor("#444444").fontSize(20).text("Invoice", 50, 160);

  generateHr(doc, 185);

  const customerInformationTop = 200;

  doc
    .fontSize(10)
    .font("Helvetica-Bold")
    .text("Invoice Number:", 50, customerInformationTop)
    .font("Helvetica")
    .text(invoice.orderDetails.invoiceId, 150, customerInformationTop)
    .text("Invoice Date:", 50, customerInformationTop + 15)
    .text(invoice.orderDetails.invoiceDate, 150, customerInformationTop + 15)
    .text("Transacion ID:", 50, customerInformationTop + 30)
    .text(invoice.orderDetails.transactionId, 150, customerInformationTop + 30)
    .text("Payment via:", 50, customerInformationTop + 45)
    .text(invoice.orderDetails.paidThrough, 150, customerInformationTop + 45)

    .font("Helvetica-Bold")
    .text(invoice.customerDetails.name, 400, customerInformationTop)
    .font("Helvetica")
    .text(invoice.customerDetails.email, 400, customerInformationTop + 15)
    .text(
      invoice.customerDetails.address.line1,
      400,
      customerInformationTop + 30
    )
    .text(
      invoice.customerDetails.address.line2,
      400,
      customerInformationTop + 45
    )
    .text(
      invoice.customerDetails.address.city +
        ", " +
        invoice.customerDetails.address.state +
        ", " +
        invoice.customerDetails.address.country,
      400,
      customerInformationTop + 60
    )
    .text(
      invoice.customerDetails.address.postalCode,
      400,
      customerInformationTop + 75
    )
    .text(invoice.customerDetails.phoneNo, 400, customerInformationTop + 90)
    .moveDown();

  generateHr(doc, 317);
}

function generateInvoiceTable(doc, invoice) {
  let i;
  const invoiceTableTop = 330;

  doc.font("Helvetica-Bold");
  generateTableRow(
    doc,
    invoiceTableTop,
    "S no",
    "Item",
    "Unit Cost",
    "Quantity",
    "Total"
  );
  generateHr(doc, invoiceTableTop + 20);
  doc.font("Helvetica");

  for (i = 0; i < invoice.orderDetails.products.length; i++) {
    const item = invoice.orderDetails.products[i];
    const position = invoiceTableTop + (i + 1) * 30;
    generateTableRow(
      doc,
      position,
      i + 1,
      item.name,
      formatCurrency(item.price),
      item.quantity,
      formatCurrency(item.price * item.quantity)
    );

    generateHr(doc, position + 20);
  }

  const subtotalPosition = invoiceTableTop + (i + 1) * 40;
  generateTableRow(
    doc,
    subtotalPosition,
    "",
    "",
    "Subtotal",
    "",
    formatCurrency(invoice.orderDetails.subTotal)
  );

  const TaxPosition = subtotalPosition + 20;
  generateTableRow(
    doc,
    TaxPosition,
    "",
    "",
    "Tax (18%)",
    "",
    formatCurrency(invoice.orderDetails.tax)
  );
  const deliveryPosition = TaxPosition + 20;
  generateTableRow(
    doc,
    deliveryPosition,
    "",
    "",
    "Delivery Charges",
    "",
    formatCurrency(invoice.orderDetails.delivery)
  );

  const totalAmountPosition = deliveryPosition + 25;
  doc.font("Helvetica-Bold");
  generateTableRow(
    doc,
    totalAmountPosition,
    "",
    "",
    "Total Amount",
    "",
    formatCurrency(invoice.orderDetails.totalAmount)
  );
  doc.font("Helvetica");
}

function generateFooter(doc) {
  doc.fontSize(10).text("Thank you for your business.", 50, 780, {
    align: "center",
    width: 500,
  });
}

function generateTableRow(doc, y, Sno, item, unitCost, quantity, lineTotal) {
  doc
    .fontSize(10)
    .text(Sno, 50, y)
    .text(item, 150, y)
    .text(unitCost, 280, y, { width: 90, align: "right" })
    .text(quantity, 370, y, { width: 90, align: "right" })
    .text(lineTotal, 0, y, { align: "right" });
}

function generateHr(doc, y) {
  doc.strokeColor("#aaaaaa").lineWidth(1).moveTo(50, y).lineTo(550, y).stroke();
}

function formatCurrency(cents) {
  return cents;
}

module.exports = {
  createInvoice,
};
