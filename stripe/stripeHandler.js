const stripe = require("stripe")(
  "sk_test_51MEXfzSGTfxVq4CQP98SMT9uGKIjOuFlXDQI1kQwhPZxftbrmr0xS9CEJ13wqLEonCYPp5RPvwAQftcNGDAPXIZd00cNk9Zng3"
);

module.exports.createCheckout = async (req, res) => {
  const items = req.body.prod;
  const lineItems = [];
  items.map((item) => {
    lineItems.push({
      price_data: {
        currency: "inr",
        product_data: {
          name: item.name,
        },
        unit_amount: item.price * 100,
      },
      quantity: item.quantity,
    });
  });

  const session = await stripe.checkout.sessions.create({
    shipping_options: [
      {
        shipping_rate_data: {
          type: "fixed_amount",
          fixed_amount: {
            amount: 10000,
            currency: "inr",
          },
          display_name: "Delivery ",
          // Delivers between 5-7 business days
          delivery_estimate: {
            minimum: {
              unit: "business_day",
              value: 3,
            },
            maximum: {
              unit: "business_day",
              value: 7,
            },
          },
        },
      },
    ],
    line_items: lineItems,
    mode: "payment",
    customer: "cus_MyUnrSobcnPQdA",
    success_url: "https://ecommercewithchat.onrender.com/success/stripe",
    cancel_url: "https://ecommercewithchat.onrender.com/cart",
  });
  res.status(200).send(session);
};

module.exports.generateInvoiceStripe = async (req, res) => {
  const events = await stripe.events.list({
    limit: 1,
  });
  if (events) {
    try {
      if (events.data[0].type === "checkout.session.completed") {
        const payment_intent_ID = events.data[0].data.object.payment_intent;
        const paymentIntent = await stripe.paymentIntents.retrieve(
          payment_intent_ID
        );
        res.status(200).send(paymentIntent);
      }
    } catch (err) {
      res.status(500).send({ message: "Something went wrong" });
    }
  }
};

module.exports.customerDetails = async (req, res) => {
  const customerId = req.params.id;
  const customer = await stripe.customers.retrieve(customerId);
  res.status(200).send(customer);
};
