const express = require("express");

const { addInvoice } = require("../controllers/invoiceController");

const invoiceRouter = express();

invoiceRouter.post("/add_invoice", (req, res) => {
  addInvoice(req, res);
});

module.exports = invoiceRouter;
