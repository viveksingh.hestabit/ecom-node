const express = require("express");
const {
  addUser,
  getAllUsers,
  getAUser,
  logoutChatUser,
  loginChatUser,
  searchChatUser,
  getCurrentChattingUser,
  refreshToken,
} = require("../controllers/userController");

const userRouter = express();

userRouter.post("/add_user", (req, res) => {
  addUser(req, res);
});

userRouter.get("/users", (req, res) => {
  getAllUsers(req, res);
});

userRouter.get("/user/:user_id", (req, res) => {
  getAUser(req, res);
});

userRouter.get("/search_user/:id", (req, res) => {
  searchChatUser(req, res);
});

userRouter.post("/login_chat_user", (req, res) => {
  loginChatUser(req, res);
});

userRouter.post("/logout_chat_user", (req, res) => {
  logoutChatUser(req, res);
});

userRouter.get("/get_chat_user/:id", (req, res) => {
  getCurrentChattingUser(req, res);
});

// userRouter.post("/refreshToken", (req, res) => {
//   refreshToken(req, res);
// });

module.exports = userRouter;
