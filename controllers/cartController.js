const userModel = require("../models/userModel");

module.exports.addToCart = async (request, response) => {
  const { product } = request.body;
  try {
    const user = await userModel.findOne(request.body.user_ID);
    if (user) {
      let cartProduct = false;
      user.cart.map((item) => {
        if (item.product_id === product.product_id) {
          cartProduct = true;
        }
      });
      if (cartProduct) {
        response.json({ message: "Already added", success: false });
      } else {
        user.cart.push(product);
        await user.save();
        response.send({ user: user, success: true, message:'Successfully added to cart' });
      }
    } else {
      response.json({ message: "please login first", success: false });
    }
  } catch (error) {
    response.status(500).send(error);
  }
};

module.exports.removeFromCart = async (request, response) => {
  const {user_ID,prod_ID}=request.body
  try {
    const user = await userModel.updateMany(
      { _id: user_ID },
      {
        $pull: {
          cart: { product_id: { $in: prod_ID.product_id } },
        },
      }
    );
    response.send(user);
  } catch (err) {
    response.send(err);
  }
};

module.exports.emptyCart = async (request, response) => {
  try {
    const user = await userModel.findOneAndUpdate(
      { ID: request.body.user_ID },
      {
        $set: { cart: [] },
      }
    );
    response.send(user);
  } catch (err) {
    response.send(err);
  }
};

module.exports.addQuantity = async (req, res) => {
  const { userId, prodId } = req.body;
  try {
    const user = await userModel.findById(userId);
    if (user) {
      const query = { _id: userId, "cart.product_id": prodId };
      const updation = {
        $inc: { "cart.$.quantity": 1 },
      };
      const result = await userModel.updateOne(query, updation);
      res.json({ result: result, success: true });
    } else {
      return res.json({ message: "please login first", success: false });
    }
  } catch (err) {
    return res.send(err);
  }
};

module.exports.subtractQuantity = async (req, res) => {
  const { userId, prodId } = req.body;
  try {
    const user = userModel.findById(userId);
    if (user) {
      const query = { _id: userId, "cart.product_id": prodId };
      const updation = {
        $inc: { "cart.$.quantity": -1 },
      };
      const result = await userModel.updateOne(query, updation);
      res.send(result);
    }
  } catch (err) {
    res.send(err);
  }
};
