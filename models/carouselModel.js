const mongoose = require("mongoose");

const CarouselSchema = new mongoose.Schema({
  description: {
    type: String,
    trim: true,
  },
  img: {
    type: String,
  },
  name: {
    type: String,
    unique: true,
  },
});

const Carousel = mongoose.model("Carousel", CarouselSchema);
module.exports = Carousel;
