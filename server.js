const express = require("express");
const mongoose = require("mongoose");
const userRouter = require("./routes/userRoutes");
const cors = require("cors");
const productRouter = require("./routes/productRoutes");
const stripeRouter = require("./stripe/stripeRoutes");
const paypalRouter = require("./paypal/paypalRoutes");
const razorpayRouter = require("./razorpay/razorpayRoutes");
const invoiceRouter = require("./routes/invoiceRoutes");
const msgRouter = require("./routes/messageRoutes");
const chatRouter = require("./routes/chatRoutes"); 
const User = require("./models/userModel");
const cartRouter = require("./routes/cartRoutes");
const carouselRouter = require("./routes/carouselRoutes");
require("dotenv").config();
const bcrypt = require ('bcrypt');
const jwt = require("jsonwebtoken");

const app = express(); 
app.use(express.json());
app.use(cors());

mongoose.connect(
  "mongodb+srv://viveksinghhestabit:viveksinghhestabit@cluster0.w3ceaov.mongodb.net/node" 
);
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
});

app.use(userRouter);
app.use(productRouter);
app.use(stripeRouter);
app.use(paypalRouter);
app.use(razorpayRouter);
app.use(invoiceRouter);
app.use(msgRouter);
app.use(chatRouter);
app.use(cartRouter);
app.use(carouselRouter);

const server = app.listen(8080, () => {
  console.log("Server is running at port 8080");
});
const io = require("socket.io")(server, {
  pingTimout: 60000,
  cors: {
    origin: "http://localhost:3000",
  },
});

io.on("connection", (socket) => {
  // console.log("Connected to socket.io");

  socket.on("setup", async (userID) => {
    socket.join(userID);
    socket.emit("connected");
    // console.log(socket.id);
    try {
      await User.findOneAndUpdate(
        { _id: userID },
        { $set: { SID: socket.id, status: true } }
      );
      socket.broadcast.emit("online");
    } catch (err) {
      console.log(err);
    }
  });

  socket.on("join chat", (room) => {
    socket.join(room);
    // console.log("User Joined Room: " + room);
  });
  socket.on("typing", (room) => socket.in(room).emit("typing"));
  socket.on("stop typing", (room) => socket.in(room).emit("stop typing"));

  socket.on("online", () => socket.broadcast.emit("online"));

  socket.on("offline", () => socket.broadcast.emit("offline"));

  socket.on("new message", (newMessageRecieved) => {
    let chat = newMessageRecieved.chat;

    if (!chat.users) return console.log("chat users not defined");

    chat.users.forEach((user) => {
      if (user._id === newMessageRecieved.sender._id) return;
      socket.in(user._id).emit("message recieved", newMessageRecieved);
    });
  });

  socket.on("disconnect", async () => {
    // console.log("disconnected", socket.id);
    try {
      await User.findOneAndUpdate(
        { SID: socket.id },
        { $set: { SID: "", status: false } }
      );
      socket.broadcast.emit("offline");
    } catch (err) {
      console.log(err);
    }
  });
});
