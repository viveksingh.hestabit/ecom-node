const express = require("express");
const {
  createCheckout,
  generateInvoiceStripe,
  customerDetails,
} = require("./stripeHandler");
const stripeRouter = express();

stripeRouter.post("/create_checkout_session", (req, res) => {
  createCheckout(req, res);
});

stripeRouter.get("/stripe/invoice", (req, res) => {
  generateInvoiceStripe(req, res);
});

stripeRouter.get("/stripe/customer/:id", (req, res) => {
  customerDetails(req, res);
});

module.exports = stripeRouter;
